package com.bezkoder.springjwt.service;

import org.springframework.data.domain.Pageable;
import java.util.ArrayList;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.bezkoder.springjwt.models.Product;
import com.bezkoder.springjwt.repository.ProductRepo;

@Service
public class ProductService {
	
	Product product;
	
	@Autowired
	ProductRepo productRepo ;
	
	
	//pagination sorting and searching
	
	public List<Product> getProduct(String keyword,Integer pageNo, Integer pageSize, String sortBy)
    {
		
	Pageable paging = PageRequest.of( pageNo, pageSize, org.springframework.data.domain.Sort.by(sortBy));
       
		
		Page<Product> pagedResult = productRepo.findAll(paging);
        if(pagedResult.hasContent()) {
            return pagedResult.getContent();
        } else {
            return new ArrayList<Product>();
        }
    }

	///Search Product By Name
	
		public List<Product> listAll(String keyword) {
			return productRepo.findAll(keyword);
		}
	
	//get all products
	
	public List<Product> getAllProduct() {
	List<Product> products = new ArrayList<>();
	productRepo.findAll().forEach(product -> products.add(product));
	return products;
	}

//get product by id
	
	public Product getByid(String id) {
	return productRepo.findById(id).get();
	}


//add product
	
	public Product save(Product product) {
	return productRepo.save(product);
	}


//delete product
	
	public void delete(String id) {
	productRepo.deleteById(id);
	}


//update product

	public void update(Product product, int id) {
	productRepo.save(product);
	}


	
	//get product by category
	
	  public ResponseEntity<List<Product>> findByCategory(String category) {
	    try {
	      List<Product> products = productRepo.findByCategory(category);
	      if (products.isEmpty()) {
	        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	      }
	      return new ResponseEntity<>(products, HttpStatus.OK);
	    } catch (Exception e) {
	      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
	    }
	  }

	
	  
	 
	  
	  //get product by name
	
	  public ResponseEntity<List<Product>> findByName(String name) {
		    try {
		      List<Product> products = productRepo.findByName(name);
		      if (products.isEmpty()) {
		        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		      }
		      return new ResponseEntity<>(products, HttpStatus.OK);
		    } catch (Exception e) {
		      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		    }
		  }






	
	





	
	}
	
	  
	 
	  
	 
/*
//pagination

public Page<Product> listAll(int pageNo){
Pageable pagable = PageRequest.of(pageNo - 1, 10);
return productRepo.findAll(pagable);
}

*/  
	  
/*	
// Product by Price in ascending order
@Override
  public List<Product> findAllProductByPriceAsc() {
        return productRepo.findAllProductByPriceAsc();
    }
  
  
	
	// Product by Price in ascending order
@Override
	  public List<Product> findAllProductByNameAsc() {
	        return productRepo.findAllProductByNameAsc();
	    }
	  */
	  

