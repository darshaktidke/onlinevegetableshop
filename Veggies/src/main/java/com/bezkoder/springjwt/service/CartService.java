package com.bezkoder.springjwt.service;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.bezkoder.springjwt.models.Cart;
import com.bezkoder.springjwt.models.Product;
import com.bezkoder.springjwt.models.User;
import com.bezkoder.springjwt.payload.response.CartBillResponse;
import com.bezkoder.springjwt.payload.response.ViewCart;
import com.bezkoder.springjwt.repository.CartRepository;


@Repository
public class CartService {
	
	@Autowired
	private CartRepository cartRepository;
	
	@Autowired 
	private ProductService productService;
	
	@Autowired 
	private UserService userService;
	
	
	public Cart addToCart(String email , String productId , Integer quantity) {
		User u = userService.findUserByEmail(email)
				.orElseThrow(() -> new RuntimeException("Error: User not found."));
		String cartId = u.getCart().getId();
		
		Cart c = cartRepository.findById(cartId).get();
		
		Product p = productService.getByid(productId);
		
		Set<Product> cartProducts = c.getProducts();
		cartProducts.add(p);
		c.setProducts(cartProducts);
		cartRepository.save(c);
		cartRepository.setQuantity(quantity, cartId , productId);
		
		return c;
		
	}
	
	public void deleteFromCart(String email , String productId) {
		User u = userService.findUserByEmail(email)
				.orElseThrow(() -> new RuntimeException("Error: User not found."));
		String cartId = u.getCart().getId();
		cartRepository.deleteFromCart(cartId, productId);
		
	}

	public List<ViewCart> viewCart(String email) {
		System.out.println("Email :" + email);
		User u = userService.findUserByEmail(email)
				.orElseThrow(() -> new RuntimeException("Error: User not found."));
		String cartId = u.getCart().getId();
		
		return cartRepository.viewCart(cartId);
		
	}
	

	
}
