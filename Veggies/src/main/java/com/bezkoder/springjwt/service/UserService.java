package com.bezkoder.springjwt.service;

import java.util.Optional;

import com.bezkoder.springjwt.models.User;

public interface UserService {

	//public Optional<User> findUserByUsername(String username);

	public Optional<User> findUserByEmail(String email);

	public Optional findUserByResetToken(String resetToken);

	public void save(User user);

}
