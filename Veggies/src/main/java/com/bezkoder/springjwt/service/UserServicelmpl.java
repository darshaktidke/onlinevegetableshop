package com.bezkoder.springjwt.service;

import java.util.Optional;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;
import org.springframework.stereotype.Service;

import com.bezkoder.springjwt.models.User;
import com.bezkoder.springjwt.repository.UserRepository;

@Service
public class UserServicelmpl implements UserService {
	@Autowired
	private UserRepository userRepository;
	
	@Override
	public Optional<User> findUserByEmail(String email)
	{
		return userRepository.findByEmail(email);
		
	}
	
//	@Override
//	public Optional<User> findUserByUsername(String username)
//	{
//		return userRepository.findByUsername(username);
//		
//	}
	
	@Override
	public Optional<User> findUserByResetToken(String resetToken) {
		return userRepository.findByResetToken(resetToken);
	}
	
	@Override
	public void save(User user) {
		userRepository.save(user);
	}

	
	
	
	
	
}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

//	public void updateResetPasswordToken(String restToken ,String email) throws UserNotFoundException
//	{
//		
//		User user =(User) userRepository.findByEmail(email);
//		if(user !=null)
//		{
//			user.setRestToken(restToken);
//			userRepository.save(user);
//		}else
//		{
//			throw new NotFoundException();
//			
//		}
//		}
//	public User getByResetPasswordToken(String restToken)
//	{
//		return userRepository.findByResetToken(restToken);
//		
//	}
//	public void UpdatePassword(User user,String newPassword)
//	{
//		BCryptPasswordEncode passwordEncoder=new  BCryptPasswordEncode();
//		
//		String encodePassword=passwordEncoder.encode(newPassword);
//		user.setPassword(encodePassword);
//		
//		user.setRestToken(encodePassword);
//		userRepository.save(user);
//	}
//	@Override
//	public User findByEmail(String email) {
//		// TODO Auto-generated method stub
//		return null;
//	}
//	@Override
//	public User findByResetToken(String resetToken) {
//		// TODO Auto-generated method stub
//		return null;
//	}
//	@Override
//	public void save(User user) {
//		// TODO Auto-generated method stub
//		
//	}

