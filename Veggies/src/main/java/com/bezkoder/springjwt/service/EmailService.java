package com.bezkoder.springjwt.service;

import org.springframework.mail.SimpleMailMessage;

public interface EmailService {
	public void sendEmail(String to, String subject, String body);
	}

