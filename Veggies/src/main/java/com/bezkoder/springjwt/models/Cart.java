package com.bezkoder.springjwt.models;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;


import org.hibernate.annotations.GenericGenerator;
import javax.persistence.JoinColumn;

@Entity
public class Cart {
	
	@Id
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "uuid")
	private String id;
	
	 @ManyToMany(fetch = FetchType.LAZY,
		      cascade = {
		          CascadeType.PERSIST,
		          CascadeType.MERGE
		      })
		  @JoinTable(name = "cart_product",
		        joinColumns = { @JoinColumn (name = "cart_id") },
		        inverseJoinColumns = { @JoinColumn(name = "product_id") })
	private Set<Product> products = new HashSet<Product>();
	 
	 
	

	public Cart(String id, Set<Product> products) {
		super();
		this.id = id;
		this.products = products;
	}

	public Cart() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Set<Product> getProducts() {
		return products;
	}

	public void setProducts(Set<Product> products) {
		this.products = products;
	}
	

	 
	
	

}


