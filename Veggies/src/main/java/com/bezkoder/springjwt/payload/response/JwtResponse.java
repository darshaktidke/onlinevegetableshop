package com.bezkoder.springjwt.payload.response;

import java.util.List;

public class JwtResponse {
  private String token;
  private String type = "Bearer";
  private String id;
 // private String username;
  private String firstname;
  private String lastname;
  private String email;
  private List<String> roles;

//  public String getAccessToken() {
//	    return token;
//	  }
//  public JwtResponse(String accessToken, String string, String username,String firstname,String lastname, String email, List<String> roles) {
//    this.token = accessToken;
//    this.id = string;
//   // this.username = username;
//    this.firstname=firstname;
//    this.lastname=lastname;
//    this.email = email;
//    this.roles = roles;
//  }
  

  public String getAccessToken() {
    return token;
  }

  public JwtResponse(String token, String id, String firstname, String lastname, String email, List<String> roles) {
	super();
	this.token = token;
	this.id = id;
	this.firstname = firstname;
	this.lastname = lastname;
	this.email = email;
	this.roles = roles;
}

public void setAccessToken(String accessToken) {
    this.token = accessToken;
  }

  public String getTokenType() {
    return type;
  }

  public void setTokenType(String tokenType) {
    this.type = tokenType;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

//  public String getUsername() {
//    return username;
//  }
//
//  public void setUsername(String username) {
//    this.username = username;
//  }

  public List<String> getRoles() {
    return roles;
  }

public String getFirstname() {
	return firstname;
}

public void setFirstname(String firstname) {
	this.firstname = firstname;
}

public String getLastname() {
	return lastname;
}

public void setLastname(String lastname) {
	this.lastname = lastname;
}
  
  
}
