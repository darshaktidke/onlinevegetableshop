package com.bezkoder.springjwt.payload.response;

public class CartBillResponse {
	
	private String name;
	private Double price;
	private Integer quantity;
	private String unit;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	public CartBillResponse(String name, Double price, Integer quantity, String unit) {
		super();
		this.name = name;
		this.price = price;
		this.quantity = quantity;
		this.unit = unit;
	}
	public CartBillResponse() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	

}
