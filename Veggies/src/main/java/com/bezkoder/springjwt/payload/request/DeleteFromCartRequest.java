package com.bezkoder.springjwt.payload.request;

public class DeleteFromCartRequest {
	
	private String email;
	private String productId;
	
	
	public DeleteFromCartRequest() {
		super();
		// TODO Auto-generated constructor stub
	}
	public DeleteFromCartRequest(String email, String productId) {
		super();
		this.email = email;
		this.productId = productId;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getProductId() {
		return productId;
	}
	public void setProductId(String productId) {
		this.productId = productId;
	}
	
	
}
