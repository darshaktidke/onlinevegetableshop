package com.bezkoder.springjwt.payload.response;

public interface ViewCart {

	
	String getName();
	Double getPrice();
	Integer getQuantity();
	String getUnit();
}
