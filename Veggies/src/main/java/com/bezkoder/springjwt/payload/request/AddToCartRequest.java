package com.bezkoder.springjwt.payload.request;

public class AddToCartRequest {
	
	
	private String email;
	private String productId;
	private Integer quantity;
	public AddToCartRequest(String email, String productId, Integer quantity) {
		super();
		this.email = email;
		this.productId = productId;
		this.quantity = quantity;
	}
	
	
	public AddToCartRequest() {
		super();
		// TODO Auto-generated constructor stub
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getProductId() {
		return productId;
	}
	public void setProductId(String productId) {
		this.productId = productId;
	}
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	
	

}
