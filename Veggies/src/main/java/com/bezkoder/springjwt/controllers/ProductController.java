package com.bezkoder.springjwt.controllers;


import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bezkoder.springjwt.models.Product;
import com.bezkoder.springjwt.repository.ProductRepo;
import com.bezkoder.springjwt.service.ProductService;



@RestController
@RequestMapping("/api")
public class ProductController {
	@Autowired
	ProductService productService;
	@Autowired
	ProductRepo productRepo; 
	
	// Searching Sorting and Pagination 
	
	///http://localhost:8080/product?pageSize=5&pageNo=1&sortBy=name
	
	 @GetMapping("/product")
	    public ResponseEntity<List<Product>> getProduct(    
	    		@Param("Apple") String keyword,
	    		  @RequestParam( defaultValue = "2") Integer pageNo,
	                        @RequestParam(defaultValue= "10") Integer pageSize,
	                        @RequestParam(defaultValue="id") String sortBy) 
	    {
		 List<Product> listProducts = productService.listAll(keyword);
		
		if(keyword == null) {
		 
		 List<Product> list = productService.getProduct(keyword,pageNo, pageSize, sortBy);
		
		   return new ResponseEntity<List<Product>>(list, new HttpHeaders(), HttpStatus.OK); 
	     
	    }
		else {
			   return new ResponseEntity<List<Product>>(listProducts, new HttpHeaders(), HttpStatus.OK); 
			
		}
		
		}
	
	//get all product
	
@GetMapping("/allproduct")
private List<Product> getAllProduct(){
return productService.getAllProduct();
}


//get product by id

@GetMapping("/product/{id}")
private Product getProductById(@PathVariable("id") String id ) {
return productService.getByid(id);
}



//add product

@PostMapping("/product/add")
private String saveProduct(@RequestBody Product product) {
productService.save(product);
return product.getId();
}



//update product

@PutMapping("/product/put")
private Product update(@RequestBody Product product) {
productService.save(product);
return product;
}


//delete product

@DeleteMapping("/product/delete/{id}")
private void delete(@PathVariable("id") String id) {
productService.delete(id);
}



//get Product by category

@GetMapping("/product/getbycategory/{category}")
public ResponseEntity<List<Product>> findByCategory(@PathVariable("category") String category){
	return productService.findByCategory(category);
	
}




//get Product by name

@GetMapping("/product/getbyname/{name}")
public ResponseEntity<List<Product>> findByName(@PathVariable("name") String name){
	return productService.findByName(name);
	
}




}








//pagination

/*	@RequestMapping("/")
	public String ViewHomePage(Model model) {
		int currentPage = 1;
		return listByPage(model,1);
	}
	
	@GetMapping("/product/pageno/{pageNo}")
	public String listByPage(Model model , @PathVariable("pageNo") int currentPage) {
		Page<Product> page = productService.listAll(currentPage);
		long totalItems = page.getTotalElements();
		int totalPages =page.getTotalPages();
		
		List<Product> listProducts = page.getContent();
		
		model.addAttribute("CurrentPage : " ,currentPage);
		model.addAttribute("TotalItems : " ,totalItems);
		model.addAttribute("TotalPages : " ,totalPages);
		model.addAttribute("CurrentPage : " ,currentPage);
		model.addAttribute("listProducts : " ,listProducts); 
		return "index" ;
	}
	
	//Search Product by name 

@GetMapping("/searchbyname")
public String SearchByName(Model model, @Param("keyword") String keyword) {
	List<Product> listProducts = productService.listAll(keyword);
	model.addAttribute("listProducts", listProducts);
return "index";
	
}   */

	

/*

//get product list by ascending price

@GetMapping(value = "/product/price/ascending")
public List<Product> getProductByPrice() {

  return productService.findAllProductByPriceAsc();
}




/*

//get product list by ascending name

@GetMapping(value = "/product/name/ascending")
public List<Product> getProductByName() {

return productService.findAllProductByNameAsc();
}

*/



	