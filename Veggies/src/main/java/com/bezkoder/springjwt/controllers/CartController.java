package com.bezkoder.springjwt.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bezkoder.springjwt.models.Cart;
import com.bezkoder.springjwt.models.User;
import com.bezkoder.springjwt.payload.request.AddToCartRequest;
import com.bezkoder.springjwt.payload.request.DeleteFromCartRequest;
import com.bezkoder.springjwt.payload.response.MessageResponse;
import com.bezkoder.springjwt.service.CartService;

@RestController
@RequestMapping("/api")
public class CartController {
	
	@Autowired
	private CartService cartService;
	
	@PostMapping("/cart/add")
	public ResponseEntity<?> addToCart(@RequestBody AddToCartRequest req) {
		
		cartService.addToCart(req.getEmail(), req.getProductId(), req.getQuantity());
		return ResponseEntity.ok(new MessageResponse("Item Added to Cart"));
		
	}
	
	@PostMapping("/cart/delete")
	public ResponseEntity<?> deleteFromCart(@RequestBody DeleteFromCartRequest req) {
		
		cartService.deleteFromCart(req.getEmail(), req.getProductId());
		return ResponseEntity.ok(new MessageResponse("Item deleted"));
	}
	
	
	@PostMapping("/cart/view")
	public ResponseEntity<?> viewCart(@RequestBody User u){
		
		return ResponseEntity.ok(cartService.viewCart(u.getEmail()));
	}
	
	
	

}
