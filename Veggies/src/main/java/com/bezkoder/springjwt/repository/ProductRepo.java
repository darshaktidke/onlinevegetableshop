package com.bezkoder.springjwt.repository;

import java.util.List;

import javax.persistence.EntityManager;

import javax.persistence.criteria.CriteriaBuilder;




import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;

import com.bezkoder.springjwt.models.Product;

import org.springframework.data.jpa.repository.Query;

import org.springframework.data.repository.PagingAndSortingRepository;


@Repository
public interface ProductRepo extends PagingAndSortingRepository<Product, String>{

	
	 
	public  EntityManager entityManager = null;
	 public  CriteriaBuilder criteriaBuilder = null;
	
	
	 
	  List<Product> findByCategory(String category);
	 List<Product> findByName(String name);
	 
	 @Query("FROM Product ORDER BY price ASC")
	    List<Product> findAllProductByPriceAsc();
	
	 @Query("FROM Product ORDER BY name ASC")
	    List<Product> findAllProductByNameAsc();
	
	 @Query("SELECT p FROM Product p WHERE p.name LIKE %?1%")
	    List<Product> findAll(String keyword);
	 
	 Sort sort = Sort.by("name").ascending();
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 

	
	 
	

}