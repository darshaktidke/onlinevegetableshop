package com.bezkoder.springjwt.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.bezkoder.springjwt.models.Cart;
import com.bezkoder.springjwt.payload.response.CartBillResponse;
import com.bezkoder.springjwt.payload.response.ViewCart;

public interface CartRepository extends JpaRepository<Cart,String> {
	@Modifying
	@Transactional
	@Query(value="UPDATE cart_product SET quantity= ?1 WHERE cart_id= ?2 "
			+ "AND product_id= ?3  " , 
			nativeQuery = true)
	public Integer setQuantity(Integer quantity , String cart_id , String prod_id);
	
	@Modifying
	@Transactional
	@Query(value="DELETE FROM cart_product WHERE cart_id= ?1 AND product_id= ?2 " , 
			nativeQuery = true)	
	public Integer deleteFromCart(String email , String productId);
	

	@Query(value="select p.name , p.price , cp.quantity , p.unit from cart_product "
			+ "AS cp inner join product AS p on p.id = cp.product_id "
			+ "where cp.cart_id= ?1" , 
			nativeQuery = true)	
	public List<ViewCart> viewCart(String cartId);

}
